package com.example.library.features.add.presentation;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerFragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.library.R;
import com.example.library.core.base.DataStates;
import com.example.library.features.librarylist.domain.model.Author;
import com.example.library.features.librarylist.domain.model.Book;
import com.example.library.features.librarylist.presentation.BookListViewModel;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class AddBookFragment extends DaggerFragment {

    private TextInputLayout tlIsbn;
    private TextInputLayout tlTitle;
    private TextInputLayout tlAuthor;
    private TextInputLayout tlCategory;
    private TextInputLayout tlDate;
    private TextInputLayout tlEditorial;
    private TextInputLayout tlPages;
    private TextInputLayout tlDescription;
    private TextInputLayout tlImage;

    private TextInputEditText etIsbn;
    private TextInputEditText etTitle;
    private TextInputEditText etAuthor;
    private TextInputEditText etCategory;
    private TextInputEditText etDate;
    private TextInputEditText etEditorial;
    private TextInputEditText etPages;
    private TextInputEditText etDescription;
    private TextInputEditText etImage;

    @Inject
    ViewModelProvider.Factory vmFactory;

    private AddBookViewModel addBookVM;


    public AddBookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        addBookVM = ViewModelProviders.of(this, vmFactory).get(AddBookViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_book, container, false);
        setViews(rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addBookVM.getBookAddLivedata().observe(this, booleanDataStates -> {
            switch (booleanDataStates.getStatus()) {

                case LOADING:
                    //TODO: Handle loading screen
                    break;

                case SUCCESS:
                    //TODO: Handle success screen
                    break;

                case ERROR:
                    //TODO: Handle error screen
                    break;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_save:
                if(isDataValid()) {
                    addBookVM.addBook(getBookfromForm());
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isDataValid() {

        if(!checkIfEmptyForm(createEditextList())) {
            return false;
        }

        if(etIsbn.getText().toString().trim().length() < 10) {
            setError(tlIsbn, "ISBN is not valid");
            return false;
        }

        return true;
    }

    private void setViews(View v) {
        tlIsbn = v.findViewById(R.id.tl_isbn);
        tlTitle = v.findViewById(R.id.tl_title);
        tlAuthor = v.findViewById(R.id.tl_author);
        tlCategory = v.findViewById(R.id.tl_category);
        tlDate = v.findViewById(R.id.tl_date);
        tlEditorial = v.findViewById(R.id.tl_editorial);
        tlPages = v.findViewById(R.id.tl_pages);
        tlDescription = v.findViewById(R.id.tl_description);
        tlImage = v.findViewById(R.id.tl_image);

        etIsbn = v.findViewById(R.id.et_isbn);
        etTitle = v.findViewById(R.id.et_title);
        etAuthor = v.findViewById(R.id.et_author);
        etCategory = v.findViewById(R.id.et_category);
        etDate = v.findViewById(R.id.et_date);
        etEditorial = v.findViewById(R.id.et_editorial);
        etPages = v.findViewById(R.id.et_pages);
        etDescription = v.findViewById(R.id.et_description);
        etImage = v.findViewById(R.id.et_image);
    }

    private void setError(TextInputLayout editText, String errorString) {
        editText.setError(errorString);
    }

    private void clearError(TextInputLayout editText) {
        editText.setError(null);
    }

    private boolean isEmpty(EditText editText) {
        String input = editText.getText().toString().trim();
        return input.length() == 0;
    }

    private boolean checkIfEmptyForm(Map<EditText, TextInputLayout> map) {

        for(Map.Entry<EditText, TextInputLayout> entry: map.entrySet()) {
            clearError(entry.getValue());
           if(isEmpty(entry.getKey())) {
               setError(entry.getValue(), "Field can't be empty");
               return false;
           }
        }
        return true;
    }

    private Map<EditText, TextInputLayout> createEditextList() {
        Map<EditText, TextInputLayout> mapForm = new LinkedHashMap<>();
        mapForm.put(etIsbn, tlIsbn);
        mapForm.put(etTitle, tlTitle);
        mapForm.put(etAuthor, tlAuthor);
        mapForm.put(etCategory, tlCategory);
        mapForm.put(etDate, tlDate);
        mapForm.put(etEditorial, tlEditorial);
        mapForm.put(etPages, tlPages);
        mapForm.put(etDescription, tlDescription);
        mapForm.put(etImage, tlImage);

        return mapForm;
    }

    private Book getBookfromForm() {

        Book book = new Book();
        Author author = new Author();
        author.setFirstName(etAuthor.getText().toString());

        book.setIsbn(etIsbn.getText().toString());
        book.setTitle(etTitle.getText().toString());
        book.setAuthor(author);
        book.setCategory(etCategory.getText().toString());
        book.setPublished(etDate.getText().toString());
        book.setEditorial(etEditorial.getText().toString());
        book.setPages(etPages.getText().toString());
        book.setDescription(etDescription.getText().toString());
        book.setImageUrl(etImage.getText().toString());

        return book;
    }

}
