package com.example.library.features.librarylist.presentation;

import com.example.library.core.base.DataStates;
import com.example.library.features.librarylist.domain.BooksProvider;
import com.example.library.features.librarylist.domain.model.Book;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public class BookListViewModel extends ViewModel {

    private final CompositeDisposable disposable = new CompositeDisposable();

    private BooksProvider booksProvider;

    private MutableLiveData<DataStates<List<Book>>> bookListLivedata;

    @Inject
    public BookListViewModel(BooksProvider booksProvider) {
        this.booksProvider = booksProvider;
    }

    public LiveData<DataStates<List<Book>>> getBookListLivedata() {
        if(bookListLivedata == null) {
            bookListLivedata = new MutableLiveData<>();
            getBooks();
        }
        return bookListLivedata;
    }

    private void getBooks() {
        disposable.add(booksProvider.getBooks()
                .doOnSubscribe(disposable -> bookListLivedata.postValue(new DataStates<List<Book>>().loading()))
                .doOnNext(books -> bookListLivedata.postValue(new DataStates<List<Book>>().success(books)))
                .subscribe(books -> bookListLivedata.postValue(new DataStates<List<Book>>().success(books)),
                        throwable -> bookListLivedata.postValue(new DataStates<List<Book>>().error(throwable))));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
