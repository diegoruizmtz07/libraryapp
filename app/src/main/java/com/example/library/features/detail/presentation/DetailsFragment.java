package com.example.library.features.detail.presentation;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import dagger.android.support.DaggerFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.library.R;
import com.example.library.features.librarylist.domain.model.Book;
import com.squareup.picasso.Picasso;


public class DetailsFragment extends DaggerFragment {

    private ImageView ivBook;
    private TextView tvTitle;
    private TextView tvAuthor;
    private TextView tvCategory;
    private TextView tvDate;
    private TextView tvPages;
    private TextView tvIsbn;
    private TextView tvDescription;

    public DetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, container, false);
        setViews(rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        displayData(DetailsFragmentArgs.fromBundle(getArguments()).getBook());
    }

    private void setViews(View v) {
        ivBook = v.findViewById(R.id.iv_book);
        tvTitle = v.findViewById(R.id.tv_title);
        tvAuthor = v.findViewById(R.id.tv_author);
        tvCategory = v.findViewById(R.id.tv_category);
        tvDate = v.findViewById(R.id.tv_date);
        tvPages = v.findViewById(R.id.tv_pages);
        tvIsbn = v.findViewById(R.id.tv_isbn);
        tvDescription = v.findViewById(R.id.tv_description);
    }

    private void displayData(Book book) {
        Picasso.get().load(book.getImageUrl()).into(ivBook);

        tvTitle.setText(book.getTitle());
        tvAuthor.setText(getString(R.string.author_details,
                book.getAuthor().getFirstName()+ " " + book.getAuthor().getLastName()));
        tvCategory.setText(getString(R.string.category_details, book.getCategory()));
        tvDate.setText(getString(R.string.date_details, book.getPublished()));
        tvPages.setText(getString(R.string.pages_details, book.getPages()));
        tvIsbn.setText(getString(R.string.isbn_details, book.getIsbn()));
        tvDescription.setText(book.getDescription());

    }
}
