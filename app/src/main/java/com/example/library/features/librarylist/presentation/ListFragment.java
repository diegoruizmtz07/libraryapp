package com.example.library.features.librarylist.presentation;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.DaggerFragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.library.R;
import com.example.library.core.base.DataStates;
import com.example.library.features.librarylist.domain.model.Book;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import javax.inject.Inject;


public class ListFragment extends DaggerFragment implements BookListAdapter.BookClickListener<Book>,
        SearchView.OnQueryTextListener,
        View.OnClickListener{

    @Inject
    ViewModelProvider.Factory vmFactory;

    private BookListViewModel bookListVM;

    private RecyclerView rvBooks;
    private BookListAdapter bookListAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private ProgressBar progress;
    private FloatingActionButton floatingActionButton;

    private List<Book> bookList;


    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        bookListVM = ViewModelProviders.of(this, vmFactory).get(BookListViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        setViews(rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bookListVM.getBookListLivedata().observe(this, new Observer<DataStates<List<Book>>>() {
            @Override
            public void onChanged(DataStates<List<Book>> listDataStates) {
                switch (listDataStates.getStatus()) {

                    case LOADING:
                        showLoadingScreen();
                        break;

                    case SUCCESS:
                        displayData(listDataStates.getData());
                        showSuccessScreen();
                        break;

                    case ERROR:
                        //TODO: Handle error view
                        break;

                }
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.search, menu);

        final MenuItem searchItem = menu.findItem(R.id.menu_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    private void setViews(View v) {
        progress = v.findViewById(R.id.progress);

        floatingActionButton = v.findViewById(R.id.floating_action_button);
        floatingActionButton.setOnClickListener(this);

        rvBooks = v.findViewById(R.id.rv_book);
        rvBooks.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(requireContext());
        rvBooks.setLayoutManager(layoutManager);
    }

    private void showLoadingScreen() {
        progress.setVisibility(View.VISIBLE);
        rvBooks.setVisibility(View.GONE);
    }

    private void showSuccessScreen() {
        progress.setVisibility(View.GONE);
        rvBooks.setVisibility(View.VISIBLE);
    }

    private void displayData(List<Book> books) {
        bookList = books;
        bookListAdapter = new BookListAdapter(bookList, this);
        rvBooks.setAdapter(bookListAdapter);
    }

    @Override
    public void onClick(Book book) {
        ListFragmentDirections.ActionListFragmentToDetailsFragment directions =
                ListFragmentDirections.actionListFragmentToDetailsFragment(book);
        NavHostFragment.findNavController(this).navigate(directions);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        bookListAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        bookListAdapter.filter(newText);
        return false;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.floating_action_button:
                NavHostFragment.findNavController(this).navigate(R.id.action_listFragment_to_addBookFragment);
                break;
        }

    }
}
