package com.example.library.features.librarylist.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Book")
public class BookEntity {

    @PrimaryKey
    public int _id;

}
