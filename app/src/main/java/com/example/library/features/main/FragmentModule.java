package com.example.library.features.main;

import com.example.library.features.add.presentation.AddBookFragment;
import com.example.library.features.detail.presentation.DetailsFragment;
import com.example.library.features.librarylist.presentation.ListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface FragmentModule {

    @ContributesAndroidInjector
    @SuppressWarnings("unused")
    ListFragment contributesListFragment();

    @ContributesAndroidInjector
    @SuppressWarnings("unused")
    DetailsFragment contributesDetailsFragment();

    @ContributesAndroidInjector
    @SuppressWarnings("unused")
    AddBookFragment contributesAddBookFragment();
}
