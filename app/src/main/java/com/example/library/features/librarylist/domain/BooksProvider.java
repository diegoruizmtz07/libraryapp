package com.example.library.features.librarylist.domain;

import com.example.library.core.di.network.ApiInterface;
import com.example.library.core.di.network.NetworkProvider;
import com.example.library.features.librarylist.domain.model.Book;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BooksProvider {

    private ApiInterface apiInterface;

    @Inject
    public BooksProvider(NetworkProvider<ApiInterface> retrofitProvider) {
        this.apiInterface = retrofitProvider.get();
    }

    public Observable<List<Book>> getBooks() {
        return apiInterface.getBooks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }
}
