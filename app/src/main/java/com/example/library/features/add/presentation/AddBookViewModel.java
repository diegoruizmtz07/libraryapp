package com.example.library.features.add.presentation;

import com.example.library.core.base.DataStates;
import com.example.library.features.add.domain.AddBook;
import com.example.library.features.librarylist.domain.model.Book;
import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public class AddBookViewModel extends ViewModel {

    private final CompositeDisposable disposable = new CompositeDisposable();

    private AddBook addBook;

    private MutableLiveData<DataStates<Boolean>> bookAddLiveData;

    @Inject
    public AddBookViewModel(AddBook addBook) {
        this.addBook = addBook;
    }

    public LiveData<DataStates<Boolean>> getBookAddLivedata() {
        if(bookAddLiveData == null) {
            bookAddLiveData = new MutableLiveData<>();
        }
        return bookAddLiveData;
    }

    public void addBook(Book book) {
        disposable.add(addBook.addBook(book)
                .doOnSubscribe(disposable -> bookAddLiveData.postValue(new DataStates<Boolean>().loading()))
                .subscribe(() -> bookAddLiveData.postValue(new DataStates<Boolean>().success(true)),
                        throwable -> bookAddLiveData.postValue(new DataStates<Boolean>().success(false))));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }

}
