package com.example.library.features.add.domain;

import com.example.library.core.di.network.ApiInterface;
import com.example.library.core.di.network.NetworkProvider;
import com.example.library.features.librarylist.domain.model.Book;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AddBook {

    private ApiInterface apiInterface;

    @Inject
    public AddBook(NetworkProvider<ApiInterface> retrofitProvider) {
        this.apiInterface = retrofitProvider.get();
    }

    public Completable addBook(Book book) {
        return apiInterface.addBook(book)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
