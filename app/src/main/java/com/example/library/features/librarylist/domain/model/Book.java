package com.example.library.features.librarylist.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Book implements Parcelable {

    private String _id;

    private String isbn;

    private String title;

    private Author author;

    private String category;

    private String published;

    private String publisher;

    private String pages;

    private String description;

    private String editorial;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("_createdOn")
    private String createdOn;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._id);
        dest.writeString(this.isbn);
        dest.writeString(this.title);
        dest.writeParcelable(this.author, flags);
        dest.writeString(this.category);
        dest.writeString(this.published);
        dest.writeString(this.publisher);
        dest.writeString(this.pages);
        dest.writeString(this.description);
        dest.writeString(this.imageUrl);
        dest.writeString(this.createdOn);
        dest.writeString(this.editorial);
    }

    public Book() {
    }

    protected Book(Parcel in) {
        this._id = in.readString();
        this.isbn = in.readString();
        this.title = in.readString();
        this.author = in.readParcelable(Author.class.getClassLoader());
        this.category = in.readString();
        this.published = in.readString();
        this.publisher = in.readString();
        this.pages = in.readString();
        this.description = in.readString();
        this.imageUrl = in.readString();
        this.createdOn = in.readString();
        this.editorial = in.readString();

    }

    public static final Parcelable.Creator<Book> CREATOR = new Parcelable.Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel source) {
            return new Book(source);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };
}
