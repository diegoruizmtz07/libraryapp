package com.example.library.features.main;

import android.content.Context;

import dagger.Binds;
import dagger.Module;

@Module(includes = FragmentModule.class)
public interface MainActivityModule {

    @Binds
    @SuppressWarnings("unused")
    Context providesMainActivityContext(MainActivity context);



}