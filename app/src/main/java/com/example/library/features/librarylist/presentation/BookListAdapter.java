package com.example.library.features.librarylist.presentation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.library.R;
import com.example.library.features.librarylist.domain.model.Book;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.BookListViewHolder> {

    @FunctionalInterface
    public interface BookClickListener<T> {
        void onClick(T arg);
    }

    private List<Book> bookList = new ArrayList<>();
    private List<Book> copyList = new ArrayList<>();
    private BookListAdapter.BookClickListener listener;

    public BookListAdapter(List<Book> bookList,
                           BookListAdapter.BookClickListener<Book> listener) {
        this.listener = listener;
        this.bookList.addAll(bookList);
        this.copyList.addAll(bookList);
    }

    @NonNull
    @Override
    public BookListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_book_row, parent, false);
        return new BookListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookListViewHolder holder, int position) {
        Book book = bookList.get(position);

        holder.tvTitle.setText(book.getTitle());
        holder.tvAuthor.setText(new StringBuilder()
                .append(book.getAuthor().getFirstName())
                .append(" ")
                .append(book.getAuthor().getLastName()).toString());
        holder.tvCategory.setText(book.getCategory());

        Picasso.get().load(book.getImageUrl())
                .into(holder.ivBook);

        holder.cvBook.setOnClickListener(v -> listener.onClick(book));

    }

    /**
     *  Filter a list of book object by string.
     *
     *  Compare if properties in a list of books match with string
     *  entered in actionBar. If String is empty, return all objects.
     *
     *  @param searchText String provided by user when search a book.
     *
     * **/

    public void filter(String searchText) {
        bookList.clear();
        if(searchText.isEmpty()){
            bookList.addAll(copyList);
        } else{
            searchText = searchText.toLowerCase();
            for(Book book: copyList){
                if(book.getTitle().toLowerCase().contains(searchText) ||
                        book.getCategory().toLowerCase().contains(searchText) ||
                        book.getAuthor().getFirstName().toLowerCase().contains(searchText) ||
                        book.getAuthor().getLastName().toLowerCase().contains(searchText) ||
                        book.getDescription().toLowerCase().contains(searchText) ||
                        book.getDescription().toLowerCase().contains(searchText)) {
                    bookList.add(book);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class BookListViewHolder extends RecyclerView.ViewHolder {

        private CardView cvBook;
        private ImageView ivBook;
        private TextView tvTitle;
        private TextView tvAuthor;
        private TextView tvCategory;

        public BookListViewHolder(@NonNull View itemView) {
            super(itemView);
            cvBook = itemView.findViewById(R.id.cv_book);
            ivBook = itemView.findViewById(R.id.iv_book);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAuthor = itemView.findViewById(R.id.tv_author);
            tvCategory = itemView.findViewById(R.id.tv_category);
        }
    }
}
