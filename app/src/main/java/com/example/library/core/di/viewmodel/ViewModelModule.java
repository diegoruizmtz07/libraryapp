package com.example.library.core.di.viewmodel;

import com.example.library.features.add.presentation.AddBookViewModel;
import com.example.library.features.librarylist.presentation.BookListViewModel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public interface ViewModelModule {

    @Binds
    @SuppressWarnings("unused")
    ViewModelProvider.Factory bindVMFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(BookListViewModel.class)
    ViewModel bindList(BookListViewModel bookListVM);

    @Binds
    @IntoMap
    @ViewModelKey(AddBookViewModel.class)
    ViewModel bindAddVM(AddBookViewModel addBookVM);
}
