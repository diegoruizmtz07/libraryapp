package com.example.library.core.di;

import com.example.library.core.di.db.AppDatabase;
import com.example.library.core.di.db.LocalDataProvider;
import com.example.library.core.di.db.RoomProvider;
import com.example.library.core.di.network.ApiInterface;
import com.example.library.core.di.network.NetworkProvider;
import com.example.library.core.di.network.RetrofitProvider;
import com.example.library.core.di.viewmodel.ViewModelModule;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module(includes = {ViewModelModule.class})
public abstract class AppModule {

    @Binds
    @Singleton
    abstract NetworkProvider<ApiInterface>
    bindsRetrofitProvider(RetrofitProvider retrofitProvider);

    @Binds
    @Singleton
    abstract LocalDataProvider<AppDatabase>
    bindsRoomProvider(RoomProvider roomProvider);

}

