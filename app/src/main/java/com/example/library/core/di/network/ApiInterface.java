package com.example.library.core.di.network;

import com.example.library.features.librarylist.domain.model.Book;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

import static com.example.library.core.di.network.ApiConstants.CONTENT_TYPE_APPLICATION_JSON;
import static com.example.library.core.di.network.ApiConstants.REQUEST_ADD_BOOK;
import static com.example.library.core.di.network.ApiConstants.REQUEST_GET_BOOKS;

public interface ApiInterface {

    /**
     * Request for fetch a list of Books
     */
    @Headers(CONTENT_TYPE_APPLICATION_JSON)
    @GET(REQUEST_GET_BOOKS)
    Observable<List<Book>> getBooks();


    /**
     * Request for fetch a list of Books
     */
    @Headers(CONTENT_TYPE_APPLICATION_JSON)
    @POST(REQUEST_ADD_BOOK)
    Completable addBook(@Body Book book);

}
