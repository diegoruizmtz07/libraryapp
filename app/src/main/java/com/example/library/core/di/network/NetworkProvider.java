package com.example.library.core.di.network;


public interface NetworkProvider<T> {
    T get();
}
