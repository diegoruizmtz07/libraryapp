package com.example.library.core.di;

import com.example.library.LibraryApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AppModule.class,
        AndroidSupportInjectionModule.class,
        ActivitiesModule.class
})
public interface ApplicationComponent extends AndroidInjector<LibraryApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(LibraryApplication application);
        ApplicationComponent build();
    }

    @Override
    void inject(LibraryApplication instance);

}
