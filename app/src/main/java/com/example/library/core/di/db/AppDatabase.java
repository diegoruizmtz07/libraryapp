package com.example.library.core.di.db;

import com.example.library.features.librarylist.data.BookEntity;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {BookEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

}
