package com.example.library.core.di.db;

public interface LocalDataProvider<T> {
    T get();
}
