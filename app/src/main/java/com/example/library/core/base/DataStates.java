package com.example.library.core.base;

import androidx.annotation.Nullable;

public class DataStates<T> {

    private Status status ;

    @Nullable
    private T data;

    @Nullable
    private Throwable error;

    public DataStates<T> loading() {
        this.status = Status.LOADING;
        this.data = null;
        this.error = null;
        return this;
    }

    public DataStates<T> success(T data) {
        this.status = Status.SUCCESS;
        this.data = data;
        this.error = null;
        return this;
    }

    public DataStates<T> error(Throwable error) {
        this.status = Status.ERROR;
        this.data = null;
        this.error = error;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public T getData() {
        return data;
    }

    public Throwable getError() {
        return error;
    }

    public enum Status {
        LOADING,
        SUCCESS,
        ERROR
    }
}
