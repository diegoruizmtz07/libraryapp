package com.example.library.core.di.db;

import android.content.Context;

import com.example.library.LibraryApplication;

import androidx.room.Room;

public class RoomProvider implements LocalDataProvider<AppDatabase> {

    private Context applicationContext;
    private AppDatabase appDatabase;

    public RoomProvider(LibraryApplication libraryApplication) {
        this.applicationContext = libraryApplication;
        appDatabase = buildDBProvider();
    }

    @Override
    public AppDatabase get() {
        return appDatabase;
    }

    private AppDatabase buildDBProvider() {
        return Room.databaseBuilder(applicationContext,
                AppDatabase.class, "library-db").build();
    }
}
