package com.example.library.core.di;

import com.example.library.features.main.MainActivity;
import com.example.library.features.main.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface ActivitiesModule {

    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    @SuppressWarnings("unused")
    MainActivity contributesMainActivity();


}

