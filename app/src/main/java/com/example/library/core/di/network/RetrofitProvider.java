package com.example.library.core.di.network;

import com.example.library.BuildConfig;
import com.example.library.LibraryApplication;
import com.readystatesoftware.chuck.ChuckInterceptor;

import org.jetbrains.annotations.NotNull;


import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitProvider implements NetworkProvider<ApiInterface> {

    private static final String baseUrl = BuildConfig.BASE_URL;
    private ApiInterface requestInterface;

    private LibraryApplication application;

    @Inject
    public RetrofitProvider(LibraryApplication application) {
        this.application = application;
        this.requestInterface = buildRetrofit();
    }

    private ApiInterface buildRetrofit() {

        final OkHttpClient okHttpClient = buildHttpClient();

        requestInterface = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .callFactory(new Call.Factory() {
                    @NotNull
                    @Override
                    public Call newCall(@NotNull Request request) {
                        request = request.newBuilder().tag(new Object[]{null}).build();
                        Call call = okHttpClient.newCall(request);
                        ((Object[]) request.tag())[0] = call;
                        return call;
                    }
                })
                .build()
                .create(ApiInterface.class);

        return requestInterface;
    }

    private OkHttpClient buildHttpClient() {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

        okHttpBuilder.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });

        HttpLoggingInterceptor logginInterceptor =  new HttpLoggingInterceptor();
        logginInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        if(BuildConfig.DEBUG) {
            okHttpBuilder.addInterceptor(new ChuckInterceptor(application.getApplicationContext()));
            okHttpBuilder.addInterceptor(logginInterceptor);
        }

        return okHttpBuilder.build();
    }

    @Override
    public ApiInterface get() {
        return requestInterface;
    }
}
